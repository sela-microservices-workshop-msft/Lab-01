# Microservices Workshop
Lab 01: Setting up the lab infrastructure

---


# Tasks

 - Create an Azure DevOps organization
 
 - Create a Team Project for the workshop
 
 - Import the application repositories
  
 - Create the required repositories in DockerHub
 
 - Configure build agents using Docker containers
 
 - Configure Deployment Groups for our environment
 
 - Add a Docker Registry Service Connection

---

## Create an Azure DevOps organization

&nbsp;

 - Browse to the Azure DevOps main page and login:

```
https://dev.azure.com
```

&nbsp;

 - Create a new organization by clicking "New Organization":
 

<img alt="Image 1.1" src="images/task-1.1.png"  width="75%" height="75%">

&nbsp;

 - Click "Continue" to accepts the terms and conditions:
 

<img alt="Image 1.2" src="images/task-1.2.png"  width="75%" height="75%">

&nbsp;

 - Set a name and a region for your Azure DevOps organization:
 

<img alt="Image 1.3" src="images/task-1.3.png"  width="75%" height="75%">

&nbsp;

  - Wait until the organization is ready
 
&nbsp;

## Create a Team Project for the workshop

&nbsp;

 - Browse to the organization main page:

```
https://dev.azure.com/<your-organization>
```
 
 &nbsp;
 
 - Create a new project called "microservices-workshop":
 

<img alt="Image 2.1" src="images/task-2.1.png"  width="75%" height="75%">

&nbsp;

 - Ensure the project was successfully created:
 

<img alt="Image 2.2" src="images/task-2.2.png"  width="75%" height="75%">

&nbsp;

## Import the application repositories

&nbsp;

 - Browse to the "Repos" section:
 

<img alt="Image 3.1" src="images/task-3.1.png"  width="75%" height="75%">

&nbsp;

 - Click "Import Repository" from the repositories section:
 

<img alt="Image 3.2" src="images/task-3.2.png"  width="75%" height="75%">

&nbsp;

 - Set the clone URL and click Import:
 

<img alt="Image 3.3" src="images/task-3.3.png"  width="75%" height="75%">

&nbsp;

 - Repeat the process for each application repository:

   * https://github.com/selaworkshops/division-service.git
 
   * https://github.com/selaworkshops/sum-service.git
 
   * https://github.com/selaworkshops/multiplication-service.git
 
   * https://github.com/selaworkshops/subtraction-service.git
 
   * https://github.com/selaworkshops/ui-service.git
 
   * https://github.com/selaworkshops/microservices-calculator-app.git
 
&nbsp;

## Create the required repositories in DockerHub

&nbsp;

 - Browse to "DockerHub" and login:
 
```
https://hub.docker.com/
```

&nbsp;

 - Click "Create Repository":
 

<img alt="Image 4.1" src="images/task-4.1.png"  width="75%" height="75%">

&nbsp;

 - Set the "Repository Name" and click "Create":
 

<img alt="Image 4.2" src="images/task-4.2.png"  width="75%" height="75%">

&nbsp;

 - Repeat the process for each required repository:

   * division-service
 
   * sum-service
 
   * multiplication-service
 
   * subtraction-service
 
   * ui-service
 
&nbsp;

## Configure build agents using Docker containers

&nbsp;

 - In order to configure build agents using Docker containers we need to perform several things:

   * Ask the instructor for the build server IP
 
   * Create a Personal Access Token (PAT)
 
   * Configure Build Agents
   
&nbsp;

### Create a Personal Access Token (PAT)

&nbsp;

 - Open your profile and go to your security details:


<img alt="Image 5.1" src="images/task-5.1.png"  width="75%" height="75%">

 &nbsp;
 
 - Select "Personal Access Token" and click "New Token":


<img alt="Image 5.2" src="images/task-5.2.png"  width="75%" height="75%">

 &nbsp;
 
 - Create your token with "Full Access" privileges: 


<img alt="Image 5.3" src="images/task-5.3.png"  width="75%" height="75%">

 &nbsp;
 
 - When you're done, make sure to copy the token (you will not be able to see it again)
 

&nbsp;

### Configure Build Agents 	

&nbsp;

 - Browse to the admin page abd click "New agent pool" (replace <your-organization> with your organization name):
 
```
https://dev.azure.com/<your-organization>/microservices-workshop/_settings/agentqueues
```

<img alt="Image 6.1" src="images/task-6.1.png"  width="75%" height="75%">

&nbsp;

 - Create an agent pool called "build-agents": 

<img alt="Image 6.2" src="images/task-6.2.png"  width="75%" height="75%">
 
&nbsp;

 - Connect to the build server using ssh:
 
```
$ ssh vsts@<SERVER-IP>
```

&nbsp;

 - If you are prompted with message as below, write "yes":
 
```
The authenticity of host '123.123.123.123 (123.123.123.123)' can't be established.
ECDSA key fingerprint is SHA256:2eCG85123djThMZ123cwC1238fEFrcQFNRj123ioxGk.
Are you sure you want to continue connecting (yes/no)?

$ yes
```

&nbsp;

 - Enter the password to connect:
 
```
$ <password>
```

&nbsp;

 - Pull the vsts agent image:
 
```
$ docker pull leonjalfon1/vsts-agent
```

&nbsp;

 - Deploy 5 agents by running the command below 5 times (and replacing the <parameters>):
 
```
$ docker run -d \
--restart always \
-e VSTS_ACCOUNT=<your-organization> \
-e VSTS_TOKEN=<your-pat-token> \
-e VSTS_AGENT='$(hostname)-agent' \
-e VSTS_POOL=build-agents \
-e VSTS_WORK='/var/vsts/$VSTS_AGENT' \
-v /var/vsts:/var/vsts \
-v /var/run/docker.sock:/var/run/docker.sock \
leonjalfon1/vsts-agent
``` 
 
 &nbsp;
 
 - Ensure the agents containers are up and running:
 
```
$ docker ps
```

&nbsp;

 - You should see something like the below:
 
```
CONTAINER ID        IMAGE                    COMMAND             CREATED             STATUS              PORTS               NAMES
2c3a90baebaf        leonjalfon1/vsts-agent   "./start.sh"        2 minutes ago       Up 13 seconds                           mystifying_lewin
ed48b7b8d74f        leonjalfon1/vsts-agent   "./start.sh"        2 minutes ago       Up 13 seconds                           youthful_elion
7e82f85b0119        leonjalfon1/vsts-agent   "./start.sh"        2 minutes ago       Up 13 seconds                           eloquent_almeida
055a466b2683        leonjalfon1/vsts-agent   "./start.sh"        2 minutes ago       Up 13 seconds                           kind_poitras
ae51c162df18        leonjalfon1/vsts-agent   "./start.sh"        2 minutes ago       Up 13 seconds                           boring_hugle
```

&nbsp;

 - Ensure the agents were successfully configured from the admin page:
 

<img alt="Image 6.3" src="images/task-6.3.png"  width="75%" height="75%">

&nbsp;

 - Disconnect from the build server to avoid unintentional mistakes:

```
$ exit
```
 
&nbsp;

## Configure Deployment Groups	

&nbsp;

 - Ask the instructor for the environments servers IP's
  
 - Repeat the process below for each environment:

   * Development Environment
   
   * Staging Environment
 
   * Production Environment
   

&nbsp;

 - Browse to the deployment groups page:

```
https://dev.azure.com/<your-organization>/microservices-workshop/_machinegroup
```

<img alt="Image 7.1" src="images/task-7.1.png"  width="75%" height="75%">
 
&nbsp;

- Click "New" to create a new deployment group:

<img alt="Image 7.2" src="images/task-7.2.png"  width="75%" height="75%">
 
&nbsp;

- Set the group "Name" and click "Create":

 <img alt="Image 7.3" src="images/task-7.3.png"  width="75%" height="75%">
 
&nbsp;

 - Set "Linux" as "type of target", check the box "use personal access token..." and click "copy script to the clipboard":

<img alt="Image 7.4" src="images/task-7.4.png"  width="75%" height="75%">

&nbsp;

- Connect to the server and run the copied script:

```
$ ssh vsts@<SERVER-IP>
$ <script>
```

&nbsp;

- You will be prompted for add deployment group tags, enter "n":

```
Enter deployment group tags for agent? (Y/N) (press enter for N)
$ n
```

&nbsp;

- Disconnect from the build server to avoid unintentional mistakes:

```
$ exit
```

&nbsp;

- Browse to "Targets" to ensure the server was succesfully added to the group:

<img alt="Image 7.5" src="images/task-7.5.png"  width="75%" height="75%">

&nbsp;

## Add a Docker Registry Service Connection	

&nbsp;

 - Create a new "docker registry" service connection in the project settings portal:

  
<img alt="Image 8.1" src="images/task-8.1.png"  width="75%" height="75%">


&nbsp;

 - Select "Docker Hub" as type, Set "DockerHub" as name and enter your dockerhub credentials:

<img alt="Image 8.2" src="images/task-8.2.png"  width="50%" height="50%">
 
(Click "verify this connection" to ensure the connection success)
 
&nbsp;
 